#ifndef	_XT_WX_INCLUDE_H_
#define	_XT_WX_INCLUDE_H_

/*Netfilter插件，识别微信*/
#include <linux/types.h>
#define	XT_WX_MAX_OP_NUM		10
#define	XT_WX_MAX_STR_NUM		2
#define	XT_WX_MAX_STR_LENG		25
#if 0
struct xt_wx_info {
    u_int32_t pos;
    u_int32_t val;
    u_int16_t flags;
};
#else
#define	WX_STR_T		1
#define	WX_CODE_T	    2
struct xt_wx_string_info {
	char 		operation_code[XT_WX_MAX_STR_LENG];
};

struct xt_wx_uint32_info {
	u_int32_t	operation_code;
};

struct xt_wx_info {
    union {
    struct xt_wx_uint32_info op_code;
    struct xt_wx_string_info op_str;
    };
    u_int8_t wx_op_type;
};
#endif

typedef struct wx_header_s {
    u_int32_t packet_len; /* 前4字节表示数据包长度，可变* */
    u_int16_t header_len; /*2个字节表示头部长度,固定值，0x10*/
    u_int16_t thx_ver; /*2个字节表示谢意版本，固定值，0x01*/
    u_int32_t operation_code; /*4个字节操作说明数字，可变*/
    u_int32_t serial_number; /*序列号，可变*/
}wx_header_t;

#endif
