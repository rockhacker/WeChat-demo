XT_WX_MODULE_NAME = xt_wx

XT_WX_INSTALL_DIR = /home/kendo/ipwall/rootfs/lib/modules/3.2.28/kernel/net/netfilter
XT_WX_TEST_INSTALL_DIR = /home/kendo/ipwall/rootfs/lib/modules/3.2.28/kernel/net/netfilter
KERNEL_SRC = /lib/modules/3.2.28/build

obj-m := $(XT_WX_MODULE_NAME).o

all:
	$(MAKE) -C $(KERNEL_SRC) M=$(PWD) modules
	
install:all
	$(shell [ -d ${XT_WX_INSTALL_DIR} ] || mkdir -p ${XT_WX_INSTALL_DIR})
	
	rm -rf $(XT_WX_INSTALL_DIR)/$(XT_WX_MODULE_NAME).ko
	cp $(XT_WX_MODULE_NAME).ko $(XT_WX_INSTALL_DIR) 
	
clean:uninstall
	rm -rf *.ko *.o *.mod.c  Module.symvers modules.order .*.cmd .tmp_versions

uninstall:
	rm -rf $(XT_WX_INSTALL_DIR)/$(XT_WX_MODULE_NAME).ko

